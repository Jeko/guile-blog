(use-modules (haunt asset)
             (haunt site)
	     (haunt html)
	     (haunt page)
	     (haunt utils)
             (haunt builder blog)
	     (haunt reader commonmark)
             (haunt reader skribe)
	     (haunt builder assets))

(define (link name uri)
  `(a (@ (href ,uri)) ,name))

(define (anchor content uri)
  `(a (@ (href ,uri)) ,content))

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
	       (with-layout jeko-theme site title body)
	       sxml->html)))

(define carnets-page
  (static-page
   "Carnets"
   "carnets.html"
   `((h1 "Carnets")
     (p ,(anchor "Guile - Installation" "carnets/guile-install.html")
        " — Comment installer Guile sur votre système"))))

(define* (carnet-page #:key file-name title content)
  (define body
    `((h1 ,title)
      ,content))
  (static-page title (string-append "carnets/" file-name) body))

(define guile-install-page
  (carnet-page
   #:file-name "guile-install.html"
   #:title "Installer Guile"
   #:content
   `((h2 "Pour GNU/Linux")
     (h3 "Avec Guix (recommandé)")
     (h4 "Prérequis")
     (p "Si Guix n'est pas installé sur ta machine, exécutes les commandes suivantes dans ta console :")
     (pre "$ cd /tmp")
     (pre "$ wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh")
     (pre "$ chmod +x guix-install.sh")
     (pre "$ sudo ./guix-install.sh")
     (h4 "Installation")
     (p "Une fois que Guix est installé, tu peux installer Guile avec la commande suivante :")
     (pre "$ guix install guile")
     (p "Quand tout est terminé, tu peux démarrer un interpréteur Guile :")
     (pre "$ guile")
     (img (@ (src "/images/base-interpreter.png")))
     (h4 "Configuration")
     (p "L'interpréteur Guile livré est très spartiate. J'utilise deux modules (un pour naviguer dans l'historique des évaluations et l'autre pour la coloration syntaxique) qui le rendent, à mon sens, plus agréable à utiliser. Pour en bénéficier, tu dois installer ces deux modules :")
     (pre "$ guix package -i guile-readline guile-colorized")
     (p "Ensuite, il faut créer (ou modifier) le fichier de configuration de ton interpréteur (qui doit se trouver ici : ~/.guile) pour qu'il contienne l'expression suivante :")
     (pre "(use-modules (ice-9 readline) (ice-9 colorized)) (activate-readline) (activate-colorized)")
     (p "Si tu démarre un nouvel interpréteur, tu devrais voir notamment un peu de couleur ainsi que la possibilité de remonter dans l'historique des expressions que tu as évalué.")
     (pre "$ guile")
     (img (@ (src "/images/enhance-interpreter.png"))))))

(define (jeko-default-layout site title body)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1")))
     (title ,(string-append title " — " (site-title site))))
    (body
     (h1 ,(link (site-title site) "/"))
     (div (@ (class "nav"))
     	  (ul (li ,(link "Carnets" "/carnets.html"))))
     ,body)))

(define jeko-theme
  (theme #:name "Jeko-Default"
         #:layout jeko-default-layout))

(site #:title "Emerger des parenthèses"
      #:domain "jeko.dev"
      #:default-metadata
      '((author . "Jérémy Korwin")
        (email  . "jeremy@korwin-zmijowski.fr"))
      #:readers (list commonmark-reader skribe-reader)
      #:builders (list (blog #:theme jeko-theme)
		       carnets-page
		       guile-install-page
		       (static-directory "images")))
