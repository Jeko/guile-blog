title: Les videos d'Andy Balaam
date: 2019-08-18 18:00:00
tags: scheme video andy balaam
---

# Feel the cool avec Andy Balaam !

Les vidéos d'[Andy Balaam](http://www.artificialworlds.net/) sur la programmation avec le langage
Scheme. Je les ai trouvé inspirantes et en plus on peut les regarder sur
Peertube !

Vous pouvez même suivre l'auteur sur [Mastodon](https://mastodon.social/@andybalaam).

Aller, histoire de meubler cet article, la première vidéo de la série sous
vos yeux ébahis :

[![Scheme 1: Feel the cool - Andy Balaam](https://peertube.mastodon.host/static/thumbnails/6bdf3ade-efbd-435b-9334-9b10da0cf5cf.jpg)](https://peertube.mastodon.host/videos/watch/6bdf3ade-efbd-435b-9334-9b10da0cf5cf "Scheme 1: Feel the cool")
