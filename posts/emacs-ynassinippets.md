title: Utiliser des snippets dans Emacs
date: 2020-01-26 00:00
tags: emacs productivité ynas template snippets
---

Je cherchais un moyen de créer des templates de fichiers dans
[Emacs](https://www.gnu.org/software/emacs/) pour m'éviter des
copier/coller lorsque je veux écrire un nouvel article de blog, ou démarrer
un kata de programmation (en [Guile](https://www.gnu.org/software/guile/), bien sûr). Je suis alors tombé sur le
[blog de Howard Abrams](http://howardism.org) et son article intitulé
[«Having Emacs Type for
You»](http://howardism.org/Technical/Emacs/templates-tutorial.html). Dans
cet article, Howard nous offre un tutorial sur l'utilisation de
[YAS](https://github.com/joaotavora/yasnippet), un système de template pour
Emacs. Grâce à ses explications très simples, je me suis rapidement créé
quelques templates pour :
* Insérer un squelette d'articles pour mon blog quand je tape `blog ↹`
* Insérer une suite de test en Guile pour faire un petit kata quand je tape `kata-guile ↹`
* Insérer la licence GPL en commentaire quand je tape `GPl ↹`

Les fonctionnalités ne s'arrêtent pas à l'insertion pure et dure de texte,
je peux spécifier les emplacements où placer le curseur pour guider
l'édition du template. Je peux aussi insérer des variables dans le template
comme [la date du jour](https://www.emacswiki.org/emacs/InsertingTodaysDate). La liste est encore longue, mais je m'arrête ici car
c'est tout ce dont j'avais besoin pour aujourd'hui.

Si comme moi vous avez besoin d'un petit système de template, jetez un
oeil à [YAS](https://github.com/joaotavora/yasnippet) !
