title: Empaqueter ac-geiser pour Guix #1
date: 2020-02-26 22:10
tags: guix paquet pratique
---

Premier volet de la série sur la création de ma première définition de
paquet pour le gestionnaire de paquet Guix ! J'ai choisi
[ac-geiser](https://github.com/xiaohanyu/ac-geiser) car j'utilise cette
extension pour apporter un soupçon d'auto-complétion à
[Geiser](https://www.nongnu.org/geiser/) lorsque j'*hack* avec
[Guile](https://www.gnu.org/software/guile/) dans
[Emacs](https://www.gnu.org/software/emacs/) (et puis surtout parce que son
paquet n'existait pas à ce jour) !

Dans cette série, je documente ma méthode d'apprentissage de la création de
paquet pour [Guix](https://guix.gnu.org/). C'est une méthode qui le veut
axée sur la pratique. C'est le retour de la console qui guide notre besoin
de recourir à la documentation. 

Quels sont les pré-requis sont nécessaires à la compréhension de ce qui
suit ?

- Des bases en Guile ou Scheme.
- Guix doit être installé.
- On connaît les sous commandes de Guix : build et environment

Place à l'action !

Avant toute chose, je me place dans un environnement isolé virtuellement de
mon système :

`$ guix environment --pure guix`

Ensuite, j'utilise l'outil `guix build` qui va demander à Guix de
construire le paquet d'un logiciel à partir de la définition que je lui
fourni (/tmp/emacs-ac-geiser.scm). :

```
[dev]$ ./pre-inst-env guix build -f /tmp/emacs-ac-geiser.scm
guix build: error: failed to load '/tmp/emacs-ac-geiser.scm': No such file or directory
```

Guix me dit que le fichier `/tmp/emacs-ac-geiser.scm` n'existe pas. C'est
pas faux haha! Je le créé et je relance la commande :

```
[dev]$ touch /tmp/emacs-ac-geiser.scm
[dev]$ ./pre-inst-env guix build -f /tmp/emacs-ac-geiser.scm
guix build: error: #<unspecified>: not something we can build
```
Apparemment, Guix ne peut pas construire un fichier vide. Remplissons-le
avec une définition vide :

```
[dev]$ echo "(define-public ac-geiser (package))" > 
/tmp/emacs-ac-geiser.scm
[dev]$ ./pre-inst-env guix build -f /tmp/emacs-ac-geiser.scm 
ice-9/boot-9.scm:3832:12: error: package: unbound variable
hint: Did you forget `(use-modules (guix packages))'?
```

Guix ne connait pas la variable "package" donc il me signale une erreur,
mais m'indique comment la résoudre (trop gentil).

```
[dev]$ echo "(use-modules (guix packages)) (define-public ac-geiser (package))" > /tmp/emacs-ac-geiser.scm
[dev]$ ./pre-inst-env guix build -f /tmp/emacs-ac-geiser.scm 
/tmp/emacs-ac-geiser.scm:1:55: error: (package): missing field initializers (name version source build-system synopsis description license home-page)

```

Nouvelle tentative, nouvelle erreur (mais c'est comme ça qu'on apprend,
non?). 

Donc, là, Guix me dit que l'initialisation de plusieurs champs manque à
`(package)` et me précise lesquels. J'ai déjà dit que Guix est gentil ? On
rajoute les champs demandés dans notre définition et on relance. 

```
[dev]$ echo "(use-modules (guix packages)) (define-public ac-geiser (package name version source build-system synopsis description license home-page))" > /tmp/emacs-ac-geiser.scm
[dev]$ ./pre-inst-env guix build -f /tmp/emacs-ac-geiser.scm 
/tmp/emacs-ac-geiser.scm:1:55: error: name: invalid field specifier
```

Ah, on progresse ! À présent, Guix me signal que le champ `name` est mal
spécifié. Puisque je n'ai aucune idée de comment `name` doit être spécifié,
je vais aller chercher un peu d'information dans la documentation de
Guix. C'est tellement bien fait qu'il y a tout une [page de référence du
type
`package`](https://guix.gnu.org/manual/fr/html_node/Reference-des-paquets.html#R_00e9f_00e9rence-des-paquets). J'y
apprend notamment que le champ `name` attend une chaîne de
caractères. Allez, j'essaye (ATTENTION pour le paramètre de `echo`
j'utilise des apostrophes):

```
[dev]$ echo '(use-modules (guix packages)) (define-public ac-geiser (package (name "") version source build-system synopsis description license home-page))' > /tmp/emacs-ac-geiser.scm
[dev]$ ./pre-inst-env guix build -f /tmp/emacs-ac-geiser.scm 
/tmp/emacs-ac-geiser.scm:1:55: error: version: invalid field specifier
```

Pour `version`, même punition !

```
[dev]$ echo '(use-modules (guix packages)) (define-public ac-geiser (package (name "") (version "") source build-system synopsis description license home-page))' > /tmp/emacs-ac-geiser.scm
[dev]$ ./pre-inst-env guix build -f /tmp/emacs-ac-geiser.scm 
/tmp/emacs-ac-geiser.scm:1:55: error: source: invalid field specifier
```

Pour `source`, ça se complique. La suite dans le prochain article !


P:S: En formalisant tout ça, je me suis rendu compte que je me base parfois
sur des suppositions/assumptions/intuitions et que je pourrais me poser
plus de questions, être plus curieux. Par exemple :
- si le fichier de la définition passé à `guix build` n'est pas .scm ça
  fonctionne aussi, pourquoi ? Ça sert à quoi une extension de fichier en
  fait ?
- pourquoi j'ai choisi ce modèle de définition vide avec define-public et
  compagnie ? d'où vient cette intuition ? sans doute de lectures passées
  mais qu'est-ce qui les avaient motivées ? 

