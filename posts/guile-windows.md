title: Programmer en Guile dans Windows
date: 2019-08-13 08:30:00
tags: guile windows
---

Bien que je sois un fervent défenseur du libre, je ne peux pas nier avoir
découvert la programmation en utilisant MS Windows. Or, lorsque je regarde
[la page des téléchargements le Guile](https://www.gnu.org/software/guile/download/),
je ne vois aucun exécutable destiné à MS Windows.
Avec le recul permis par ma propre expérience, je me dis que Guile pourrait
être un point d'entrée dans le monde de la programmation et du logiciel
libre. Mais pour ça il faut qu'il soit accessible en dehors du logiciel
libre ! Récemment, j'ai lu qu'un hacker, [David
Thompson](https://dthompson.us/), a décidé de compiler Guile sur
Windows. 

Voila ce que je lisais le 29 juillet 2019 :

![](images/premiertoot.png)

Donc, c'est possible même si ce n'est pas encore du *prêt-à-exécuter*... 

![](images/deuxiemetoot.png)

[Lien vers les instructions](https://gist.github.com/davexunit/f509e17cd0b9b49c6188e833f11053e1)
