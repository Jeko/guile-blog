title: Personnaliser Haunt
date: 2019-08-01 08:00:00
tags: guile haunt css
---
     
J'utilise [Haunt](https://dthompson.us/projects/haunt.html) pour générer
mon blog statique. Jusqu'ici, je n'y avait apporté aucune modification au
thème, si ce n'est pour le titre et les "*readers*" qui permettent de
convertir mes articles en [Skribilo](https://www.nongnu.org/skribilo/) ou
[Markdown](https://daringfireball.net/projects/markdown/) en page HTML.

Aujourd'hui, j'ai décidé d'ajouter la balise meta viewport pour rendre la
lecture depuis un petit écran plus confortable. Le résultat est bien sûr
loin d'être parfait mais ça viendra avec le temps. Je pense que la
prochaine étape sera d'ajouter une scrollbar horizontale pour les extraits
de code.

Bibliographie
 * [La balise meta viewport sur le MDN](https://developer.mozilla.org/fr/docs/Mozilla/Mobile/Balise_meta_viewport)
 * [La balise meta viewport sur le W3C](https://www.w3schools.com/css/css_rwd_viewport.asp)
